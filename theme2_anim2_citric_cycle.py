#!/usr/bin/env python3
"this is a fun animation made with python,py2cytoscape and cytoscape. usage: ./aardappels.py [AMOUNT OF GLUCOSE MOLECULES]"

#imports
import sys
from py2cytoscape.data.cyrest_client import CyRestClient
import time
import math

#constants

#connect client
cytoscape = CyRestClient()
#delete all networks
cytoscape.network.delete_all()
#make network
network = cytoscape.network.create(name="Potasium-Sodium Pump")
#delete all old styles, make new one
cytoscape.style.delete_all()
cytoscape_style = cytoscape.style.create('new_style')
cytoscape.style.apply(cytoscape_style,network)

#classes & functions

class Node():
	"""
	Can make Nodes in cytoscape, perfect example of object-oriented programming!
	"""
	#set defaults
	node_color = "#89D0F5"
	node_shape = "ELLIPSE"
	node_loc_x = 0
	node_loc_y = 0
	node_loc_z = 0
	node_height = 50
	node_width = 50
	nodename = ""
	node_suid = 0
	node_bcolor = "BLACK"
	
	def __init__(self,nodename):
		"""
		init makes the node and grabs the view
		""" 
		self.nodename = nodename
		self.node = network.add_node(nodename)
		self.node_suid = self.node[nodename]
		self.view_id_list = network.get_views()
		self.view = network.get_view(self.view_id_list[0], format='view')
		self.view_functions = [fun for fun in dir(self.view) if not fun.startswith('_')]
		self.view_functions.sort()
		self.node_views = self.view.get_node_views()
		self.node_views_dict = self.view.get_node_views_as_dict()

	def changenode(self,color=node_color,shape=node_shape,width=node_width,height=node_height,label=nodename,bcolor=node_bcolor):
		"""
		changenode can change a few basic properties of the node
		"""
		#change self vars
		self.node_shape = shape
		self.node_color = color
		self.node_width = width
		self.node_height = height
		self.nodename = label
		self.node_bcolor = bcolor
		#change actual props
		self.view.update_node_views(visual_property='NODE_FILL_COLOR', values={self.node_suid: color})
		self.view.update_node_views(visual_property='NODE_SHAPE', values={self.node_suid: shape})
		self.view.update_node_views(visual_property='NODE_WIDTH', values={self.node_suid: width})
		self.view.update_node_views(visual_property='NODE_HEIGHT', values={self.node_suid: height})
		self.view.update_node_views(visual_property='NODE_LABEL', values={self.node_suid: label})
		self.view.update_node_views(visual_property='NODE_BORDER_PAINT', values={self.node_suid: bcolor})
		
		#uncomment print if you want node props
		#~ print(json.dumps(self.node_views_dict[self.node[self.nodename]], indent=0))

	def changenode_misc(self,node_prop,value):
		"""
		change misc properties
		"""
		self.view.update_node_views(visual_property=node_prop, values={self.node_suid: value})

	def changepos(self,x=node_loc_x,y=node_loc_y):
		"""
		changepos changes the positioning of the node in cytoscape, relative to its own location
		"""
		#change self vars
		self.node_loc_x += x
		self.node_loc_y += y
		#change loc
		self.view.update_node_views(visual_property='NODE_X_LOCATION', values={self.node_suid: self.node_loc_x})
		self.view.update_node_views(visual_property='NODE_Y_LOCATION', values={self.node_suid: self.node_loc_y})
		#uncomment print if you need locations
		#~ print(self.node_loc_x,self.node_loc_y)

	def changepos_static(self,x,y):
		"""
		changepos changes the positioning of the node in cytoscape
		"""
		#change self vars
		self.node_loc_x = x
		self.node_loc_y = y
		#change loc
		self.view.update_node_views(visual_property='NODE_X_LOCATION', values={self.node_suid: x})
		self.view.update_node_views(visual_property='NODE_Y_LOCATION', values={self.node_suid: y})

	def change_layer(self,z):
		"""
		change layer of the node
		"""
		#change self vars
		self.node_loc_z = z
		#change layer
		self.view.update_node_views(visual_property='NODE_Z_LOCATION', values={self.node_suid: self.node_loc_z})

class Edge():
	
	def __init__(self,node1,node2):
		"""
		Makes edge between two nodes.
		"""
		#get suid of nodes
		suid1 = node1.node_suid
		suid2 = node2.node_suid
		#get view of network
		self.view_id_list = network.get_views()
		self.view = network.get_view(self.view_id_list[0], format='view')
		#connect nodes
		edges = network.add_edge(suid1,suid2,"Sinewave")
		#for each edge, change property to Sinewave edge type
		for edge in edges:
			self.view.update_edge_views(visual_property='EDGE_LINE_TYPE', values={int(edge['SUID']): 'Solid'})

#functions

def pieces(steps):
	"""
	makes multiple nodes and returns them in a list
	"""
	#membraneloop
	nodelist = []
	for i in range(steps):
		nodelist.append(Node("Node"))
	return nodelist

def change_node_list(nodelist,Acolor,Alabel,i):
	"""
	change a list op nodes
	"""
	nodelist[i].changenode(color=Acolor,label=Alabel)

def edges_connect_multiple_list(listA,listB,):
	"""
	connects two lists of nodes to eachother
	"""
	for i in range(int((len(listA)+len(listB))/2)):
		Edge(listA[i],listB[i])

def edges_connect_list(nodelist):
	"""
	connect a list of nodes with eges
	"""
	for i in range(len(nodelist)-1):
		Edge(nodelist[i], nodelist[i+1])
	Edge(nodelist[0], nodelist[-1])

def move_in_steps(node,steps,stepx,stepy,delay):
	"""
	moves the nodes in steps with a delay, so it looks like an animation
	"""
	for i in range(steps):
		time.sleep(delay)
		node.changepos(x=stepx,y=stepy)

def list_move_in_steps(nodelist,steps,stepx,stepy,delay):
	"""
	moves a nodelist in steps with a delay, so it looks like an animation
	"""
	for i in range(steps):
		for node in nodelist:
			time.sleep(delay)
			node.changepos(x=stepx,y=stepy)

def make_circle(centerposition_x,centerposition_y,radius,nodelist,begin=0,part=1):
	"""
	make a circle with nodes
	"""
	single_angle = 360 / len(nodelist)
	
	for i in range(len(nodelist)):
		angle = math.radians(begin + i * single_angle * part)
		x = centerposition_x + (math.sin(-angle) * radius)
		y = centerposition_y + (math.cos(-angle) * radius)
		nodelist[i].changepos_static(x,y)

def go_around(node, steps, delay, radius, centerposition_x=0, centerposition_y=0, begin=0 ,part=1,rotation="RIGHT"):
	""" 
	make a node travel around the circle 
	"""
	#the angle at witch the node moves around the circle
	single_angle = 360/steps
	if rotation == "LEFT":
		for i in range(steps):
			angle = math.radians(begin + i * single_angle * part)
			stepx = centerposition_x + (math.sin(angle) * radius)
			stepy = centerposition_y + (math.cos(angle) * radius)
			#move node
			node.changepos_static(stepx,stepy)
			time.sleep(delay)
	
	elif rotation == "RIGHT":
		for i in range(steps):
			angle = math.radians(begin + i * single_angle * part)
			stepx = centerposition_x + (math.sin(-angle) * radius)
			stepy = centerposition_y + (math.cos(-angle) * radius)
			#move node
			node.changepos_static(stepx,stepy)
			time.sleep(delay)
	
	else:
		print('INVALID ROTATION, MUST BE EITHER "LEFT" OR "RIGHT" ')

def label_nodelist(nodelist):
	"""
	lables nodes in the circle with numbers
	"""
	inter = -1
	num = 0
	for i in nodelist:
		num += 1
		inter += 1
		label_num = str(num)
		change_node_list(nodelist,"RED",label_num,inter)

def relpos_calc(posfrom,posto):
	"""
	calculates relative position between two locations
	"""
	#add 						when I want to go from 0 to somewhere, I Just add somewhere: 0 --> -200 --> 0-200 = -200
	#add 						when I want to go from a pos to a pos I calc the difference: 200 --> 400 --> 400 -200
	#sub then reverse			when I want to go from a neg to a pos I subtract the pos and make the answer int reverse:  -200 --> 600 --> -200-600 = -800 --> --800 (=800) 
	#reverse current and add 	when I want to go from a neg to a neg I invert the current pos and add it to goto pos: -200 --> -400 --> -400 --200(+200) = -200
	#sub						when I want to go from pos to neg I just subract the pos from the neg: 600 --> -200 --> -200 -600 = -800
	#reverse					when I Want to go from a pos to zero I just take the negative of currentpos: 800 --> 0 --> -800
	#reverse					when I WANT to go from a neg to zero I just take the inverse of neg: -800 --> 0 --> --800(+800)
	
	#0 to somewhere
	if posfrom == 0: 
		relpos = posfrom + posto
	#pos to pos
	elif posfrom > 0 and posto > 0:
		relpos = posto - posfrom
	#pos to neg
	elif posfrom > 0 and posto < 0:
		relpos = posto - posfrom
	#pos to 0
	elif posfrom > 0 and posto == 0:
		relpos = -posfrom
	#neg to neg	
	elif posfrom < 0 and posto < 0:
		relpos = posto - posfrom
	#neg to pos
	elif posfrom < 0 and posto > 0:
		relpos = -(posfrom - posto)
	#neg to 0
	elif posfrom < 0 and posto == 0:
		relpos = -posfrom
		
	return relpos

def move_to(node,steps,tox,toy,delay):
	"""
	makes nodes move to a specific location from its current postion
	"""
	curx = node.node_loc_x
	cury = node.node_loc_y
	
	relx = (relpos_calc(curx,tox)) /steps
	rely = (relpos_calc(cury,toy)) /steps
	
	move_in_steps(node,steps,relx,rely,delay)

def list_move_to(nodelist,steps,tox,toy,delay):
	"""
	makes nodes in a list move to a specific location from its current postion
	"""
	nodelist_coords = []
	for node in nodelist:
		curx = node.node_loc_x
		cury = node.node_loc_y
		relx = (relpos_calc(curx,tox)) /steps
		rely = (relpos_calc(cury,toy)) /steps
		nodelist_coords.append(relx)
		nodelist_coords.append(rely)

	for i in range(steps):
		coord_count = 0
		for j in range(len(nodelist)):
			time.sleep(delay)
			move_in_steps(nodelist[j],1,nodelist_coords[coord_count],nodelist_coords[coord_count+1],delay)
			coord_count += 2

def list_change_node(nodelist,prop,value):
	"""
	change properties of al list of nodes
	"""
	for i in range(len(nodelist)): nodelist[i].changenode_misc(prop,value)

#reaction animation functions

def step0(reaction_nodes):
	"""
	step 0: acetyl-CoA to citrusacid, added: water, product: CoA
	"""

	reaction_nodes[0].changenode(color="#009900",label="Acetyl COA",shape="RECTANGLE",width=50,height=50)
	move_in_steps(reaction_nodes[0],100,-1,1,0.001)
	move_in_steps(reaction_nodes[0],300,0,1,0.001)
	reaction_nodes[1].changepos_static(0,-400)
	reaction_nodes[1].changenode(label="COA-sh",width=45,height=35,color="ORANGE",shape="ROUNDRECT")
	reaction_nodes[2].changepos_static(100,-500)
	reaction_nodes[2].changenode(label="H2O",width=30,height=30,color="WHITE",shape="ELLIPSE")
	#move water
	move_in_steps(reaction_nodes[2],100,0,1,0.001)
	move_in_steps(reaction_nodes[2],100,-1,0,0.001)
	#move coa
	reaction_nodes[0].changenode(label="citroenzuur",shape="ELLIPSE",color="#e3e600")
	move_in_steps(reaction_nodes[1],200,-1,0,0.0005)
	move_in_steps(reaction_nodes[1],170,-5,0,0.001)
	#edit mainmol
	reaction_nodes[2].changepos_static(100,-800)
	move_in_steps(reaction_nodes[0],100,0,1,0.001)
	reaction_nodes[1].changepos_static(100,-800)

def step1(reaction_nodes):
	"""
	step 1: citrusacid to cis-aconietacid. product: water
	"""
	reaction_nodes[0].changenode(label="citroenzuur",shape="ELLIPSE",color="#e3e600")
	time.sleep(1)
	reaction_nodes[1].changenode(label="H2O",width=30,height=30,color="WHITE",shape="ELLIPSE")
	reaction_nodes[1].changepos_static(0,-300)
	move_in_steps(reaction_nodes[1],200,1,0,0.001)
	move_in_steps(reaction_nodes[1],170,5,0,0.001)

def step2(reaction_nodes):
	"""
	step 2: cis-aconietacid to Iso-citrusacid. added water
	"""
	#prep for step2
	reaction_nodes[0].changenode(label="cis-aconietzuur",shape="TRIANGLE",color="#b3b3ff")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=180)	
	#place other mols back to startpos
	reaction_nodes[1].changepos_static(100,-800)
	reaction_nodes[2].changepos_static(100,-800)
	
	#move water closer to mainmol
	reaction_nodes[1].changepos_static(100,-500)
	#move water to mainmol in 500 steps
	move_to(reaction_nodes[1],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	reaction_nodes[1].changepos_static(100,-800)

def step3(reaction_nodes):
	"""
	step 3: Iso-citrusacid to A-ketoglutaaracid. added NAD+, products NADH  H+  CO2 
	"""
	reaction_nodes[0].changenode(label="iso-citroenzuur",color="#b2b300",shape="ELLIPSE")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(1/9))))	
	#place somewhat close
	reaction_nodes[1].changepos_static(1000,0)
	reaction_nodes[2].changepos_static(1000,0)
	reaction_nodes[3].changepos_static(1000,0)
	
	#change labels and poot in correct pos
	reaction_nodes[1].changenode(label="NAD+",color="#3399ff",height=45,width=45,shape="DIAMOND")
	reaction_nodes[2].changenode(label="CO2",color="GREY",height=40,width=40,shape="ELLIPSE")
	reaction_nodes[3].changenode(label="H+",color="WHITE",height=20,width=20,shape="ELLIPSE")
	move_to(reaction_nodes[2],1,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0)
	move_to(reaction_nodes[3],1,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0)
	move_to(reaction_nodes[1],700,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	reaction_nodes[1].changenode(label="NADH",color="#b41aff",height=45,width=45,shape="DIAMOND")
	move_in_steps(reaction_nodes[1],100,1,0,0.001)
	move_in_steps(reaction_nodes[1],120,5,0,0.001)
	move_in_steps(reaction_nodes[3],100,1,0,0.001)
	move_in_steps(reaction_nodes[3],120,5,0,0.001)
	move_in_steps(reaction_nodes[2],100,1,0,0.001)
	move_in_steps(reaction_nodes[2],120,5,0,0.001)
	
	#place nodes back to correct pos
	for i in range(len(reaction_nodes)-1): reaction_nodes[i+1].changepos_static(1000,0)

def step4(reaction_nodes):
	"""
	step 4: A-ketoglutaaracid to barnsteenacid-CoA. added NAD+ CoA 
	"""
	reaction_nodes[0].changenode(label="A-ketoglutaarzuur",color="#b82e8a",shape="RECTANGLE")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(2/9))))	
	reaction_nodes[1].changenode(label="NAD+",color="#3399ff",height=45,width=45,shape="DIAMOND")
	reaction_nodes[2].changenode(label="COA-sh",width=45,height=35,color="ORANGE",shape="ROUNDRECT")
	reaction_nodes[3].changenode(label="CO2",color="GREY",height=40,width=40,shape="ELLIPSE")
	#move CO2 into main node
	move_to(reaction_nodes[3],1,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0)	
	#move coa-sh and NAD+ onto a far away position
	reaction_nodes[1].changepos_static(1500,0)
	reaction_nodes[2].changepos_static(1200,0)
	#move coa-sh and NAD+ into main node from a far pos
	list_move_to([reaction_nodes[1],reaction_nodes[2]],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.0005)
	
	reaction_nodes[1].changenode(label="NADH",color="#b41aff",height=45,width=45,shape="DIAMOND")
	reaction_nodes[2].changenode(label="H+",color="WHITE",height=20,width=20,shape="ELLIPSE")
	
	#
	move_in_steps(reaction_nodes[1],200,1,0.2,0.001)
	move_in_steps(reaction_nodes[2],150,1,0,0.001)
	move_in_steps(reaction_nodes[3],175,1,-0.3,0.001)
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],100,800,-800,0)
	
def step5(reaction_nodes):
	"""
	step 5: barnsteenzuur-CoA to barnsteenzuur. added GDP and Pi. products CoA and GTP
	"""
	reaction_nodes[0].changenode(label="Barnsteenzuur-Coa",color="#cc7a00",shape="ROUNDRECT")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(3/9))))
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,100,1500,0)
	#

	reaction_nodes[1].changenode(label="GDP",color="RED",shape="TRIANGLE",width=40,height=40)
	reaction_nodes[2].changenode(label="Pi",color="RED",shape="ELLIPSE",width=30,height=30)
	move_in_steps(reaction_nodes[1],1,100,-700,0)
	move_in_steps(reaction_nodes[2],1,000,-900,0)
	list_move_to([reaction_nodes[1],reaction_nodes[2]],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	time.sleep(0.3)
	reaction_nodes[1].changenode(label="GTP",color="#d580ff",shape="TRIANGLE",width=40,height=40)
	reaction_nodes[2].changenode(label="COA-sh",width=45,height=35,color="ORANGE",shape="ROUNDRECT")
	move_in_steps(reaction_nodes[1],80,0.8,1,0.001)
	move_in_steps(reaction_nodes[2],80,0,1,0.001)
	time.sleep(0.3)
	list_move_to([reaction_nodes[1],reaction_nodes[2]],100,1300,0,0.001)

	time.sleep(0.1)

def step6(reaction_nodes):
	"""
	step 6: barnsteenacid to fumaracid. added Q, product QH2
	"""
	#ready for step6
	reaction_nodes[0].changenode(label="Barnsteenzuur",color="#ffa31a",shape="ROUNDRECT")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(4/9))))
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,100,1200,0)
	#
	reaction_nodes[1].changenode(label="FAD",shape="OCTAGON",color="#b2b300",width=40,height=40)
	move_to(reaction_nodes[1],1,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y+200,0.001)
	move_to(reaction_nodes[1],100,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	time.sleep(0.3)
	reaction_nodes[1].changenode(label="FADH2",shape="OCTAGON",color="#e3e600",width=40,height=40)
	move_to(reaction_nodes[1],100,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y+100,0.001)
	move_to(reaction_nodes[1],100,1200,1200,0.001)
	
def step7(reaction_nodes):
	"""
	step 7: fumaracid to malaat. added water 
	"""
	reaction_nodes[0].changenode(label="Fumaarzuur",color="#7a7a52",shape="HEXAGON")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(5/9))))
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,-400,1200,0)
	#label nodes
	reaction_nodes[1].changenode(label="H2O",width=30,height=30,color="WHITE",shape="ELLIPSE")
	#move nodes
	move_to(reaction_nodes[1],1,reaction_nodes[0].node_loc_x-200,reaction_nodes[0].node_loc_y+500,0.001)
	move_to(reaction_nodes[1],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,-1000,0,0)

def step8(reaction_nodes):
	"""
	step 8: malaat to oxaloacetaat. added NAD+ product NADH H+
	"""
	reaction_nodes[0].changenode(label="maltaat",color="#1a8cff",shape="HEXAGON")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(6/9))))
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,-1000,0,0)

	reaction_nodes[1].changenode(label="NAD+",color="#3399ff",height=45,width=45,shape="DIAMOND")
	reaction_nodes[2].changenode(label="H+",color="WHITE",height=20,width=20,shape="ELLIPSE")
	move_to(reaction_nodes[2],1,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0)
	move_to(reaction_nodes[1],400,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	reaction_nodes[1].changenode(label="NADH",color="#b41aff",height=45,width=45,shape="DIAMOND")
	time.sleep(0.3)
	move_to(reaction_nodes[2],100,reaction_nodes[0].node_loc_x-100,reaction_nodes[0].node_loc_y,0.001)
	move_to(reaction_nodes[1],100,reaction_nodes[0].node_loc_x-90,reaction_nodes[0].node_loc_y-60,0.001)
	list_move_to([reaction_nodes[1],reaction_nodes[2]],80,-1000,-400,0.001)
	
def step9(reaction_nodes):
	"""
	step 9: oxaloacetaat to citrusacid. added acetyl-CoA and water, products COA
	"""
	reaction_nodes[0].changenode(label="Oxaloatelaat",color="#aa00ff",shape="OCTAGON")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(7/9))))
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,-1000,300,0)
	#
	reaction_nodes[1].changenode(label="H2O",width=30,height=30,color="WHITE",shape="ELLIPSE")
	reaction_nodes[2].changenode(color="#009900",label="Acetyl COA",shape="RECTANGLE",width=50,height=50)
	#move nodes into mainmol
	move_to(reaction_nodes[1],1,reaction_nodes[0].node_loc_x-200,reaction_nodes[0].node_loc_y-500,0.001)
	move_to(reaction_nodes[1],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	move_to(reaction_nodes[2],1,reaction_nodes[0].node_loc_x-200,reaction_nodes[0].node_loc_y-500,0.001)
	move_to(reaction_nodes[2],300,reaction_nodes[0].node_loc_x,reaction_nodes[0].node_loc_y,0.001)
	#move node out of mainmol
	reaction_nodes[2].changenode(label="COA-sh",width=45,height=35,color="ORANGE",shape="ROUNDRECT")
	move_in_steps(reaction_nodes[2],300,-1,0,0)
	move_to(reaction_nodes[2],100,-1000,300,0)
	time.sleep(0.3)
	list_move_to([reaction_nodes[1],reaction_nodes[2],reaction_nodes[3]],1,-1000,300,0)
	#place back for step1
	reaction_nodes[0].changenode(label="citroenzuur",shape="ELLIPSE",color="#e3e600")
	go_around(reaction_nodes[0], 180, 0.001, 300, part=(1/9),begin=(180+(360*(8/9))))

def counters(node,number,standardstring,mode="append",amount=1):
	"""
	Is the main function for adding a number to the label of a node, useful for NADH counter for instance
	"""

	if mode == "append":
		number += amount
		node.changenode(label=standardstring + " " + str(number))
	elif mode == "subtract":
		number -= amount
		node.changenode(label=standardstring + " " + str(number))
	else:
		print("mode has to be either 'append' or 'subtract' ")
	return number

def animation2_placing_defining():
	"""
	places the static nodes
	"""
	#circle 
	circle_nodes = pieces(9)
	edges_connect_list(circle_nodes)
	label_nodelist(circle_nodes)
	#counter
	counterframe = Node("frame")
	counterheadline = Node("frame")
	countercycles = Node("frame")
	counterFADH = Node("frame")
	counterNADH = Node("frame")
	counterCO2 = Node("frame")
	counterGTP = Node("frame")
	counterATP = Node("frame")
	

	#make reaction nodes (nodes that will represent molecules and form a reaction animation)
	reaction_nodes = pieces(4)
	#put each node in the list in a different layer
	laycount = 10
	for i in range(len(reaction_nodes)):
		reaction_nodes[i].change_layer(laycount)
		laycount -= 1
	
	"""
	change it
	"""
	#frame
	#make lists with same props
	framenodestrans = []
	framenodestrans.extend((counterheadline, counterFADH, counterNADH, counterCO2, counterGTP, countercycles,counterATP))
	framenodesfont = []
	framenodesfont.extend((counterFADH, counterNADH, counterCO2, counterGTP, countercycles,counterATP))
	#make certain nodes transparent and their border transparent
	list_change_node(framenodestrans,"NODE_TRANSPARENCY",0)
	list_change_node(framenodestrans,"NODE_BORDER_TRANSPARENCY",0)
	#make certain nodes have font size 15
	list_change_node(framenodestrans,"NODE_LABEL_FONT_SIZE",15)	
	#change misc props
	counterframe.changenode(shape="RECTANGLE",width=200,height=300)
	counterframe.changenode_misc("NODE_TRANSPARENCY",0)
	counterheadline.changenode(label="PRODUCTEN")
	counterheadline.changenode_misc("NODE_LABEL_FONT_SIZE",25)
	counterFADH.changenode(label="FADH2 = 0")
	counterNADH.changenode(label="NADH + H+ = 0")
	counterCO2.changenode(label="CO2 = 0")
	counterGTP.changenode(label="GTP = 0")
	countercycles.changenode(label="Cycles = 0")
	counterATP.changenode(label="ATP = 0")
	
	"""
	positioning
	"""
	counterframe.changepos_static(0,-8)
	counterheadline.changepos_static(-5,-130)
	countercycles.changepos_static(-40,-90)
	counterFADH.changepos_static(-38,-60)
	counterNADH.changepos_static(-17,-30)
	counterGTP.changepos_static(-47,0)
	counterCO2.changepos_static(-45,30)
	counterATP.changepos_static(-45,60)
	make_circle(0,0,220,circle_nodes,begin=180,part=1)
	
	for i in range(int(len(reaction_nodes))):
		reaction_nodes[i].changepos_static(100,-800)
	
	step0(reaction_nodes)
		
	return counterFADH ,counterNADH ,counterGTP ,counterCO2 ,countercycles,reaction_nodes ,counterATP

def animation2_animate(moved_nodes,count,countF,countN,countG,countC,countATP):
	"""
	main animetion function
	"""
	counterFADH ,counterNADH ,counterGTP ,counterCO2 ,countercycles,reaction_nodes ,counterATP = moved_nodes
	#defining standard labels
	labelcyc = "Cycles ="
	labelF = "FADH2 ="
	labelN = "NADH + H+ ="
	labelG = "GTP ="
	labelC = "CO2 ="
	labelATP = "ATP ="
	#animate with all steps
	step1(reaction_nodes)
	step2(reaction_nodes)
	step3(reaction_nodes)
	
	#count NADH and CO2
	countN = counters(counterNADH,countN,labelN)
	countC = counters(counterCO2,countC,labelC)
	step4(reaction_nodes)
	#count NADH and CO2 again
	countN = counters(counterNADH,countN,labelN)
	countC = counters(counterCO2,countC,labelC)
	step5(reaction_nodes)
	#count GTP +1
	countG = counters(counterGTP,countG,labelG)
	step6(reaction_nodes)
	#count FADH2 +1
	countF = counters(counterFADH,countF,labelF)
	step7(reaction_nodes)
	step8(reaction_nodes)
	#count NADH
	countN = counters(counterNADH,countN,labelN)
	step9(reaction_nodes)
		
	#keep cyclecount and ATP count

	count = counters(countercycles,count,labelcyc)
	countATP = counters(counterATP,countATP,labelATP,amount=36)
	
	return count,countF,countN,countG,countC,countATP

#main

def main(argv=None):
	"""
	main, runs the whole program basically, its a pretty big deal.
	"""
	if argv ==None:
		argv = sys.argv 
	#work
	
	if len(argv) != 2:
		print(__doc__)
		sys.exit()
	
	movingnodes = animation2_placing_defining()
	#counters
	cyclescount = 0
	countF = 0
	countN = 0
	countG = 0
	countC = 0
	countATP = 0
	for i in range(int(argv[1])*2):
		cyclescount,countF,countN,countG,countC,countATP = animation2_animate(movingnodes,cyclescount,countF,countN,countG,countC,countATP)
		
	#test
	#~ test(2,0,0,steps=1000)
	
	return 0

if __name__ == "__main__":
	print("its alive...   maybe")
	sys.exit(main())


