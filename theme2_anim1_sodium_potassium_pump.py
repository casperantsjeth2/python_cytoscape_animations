#!/usr/bin/env python3
"this is a fun animation made with python,py2cytoscape and cytoscape, @copyright Antsje_Casper.Inc"

#imports
import sys
from py2cytoscape.data.cyrest_client import CyRestClient
import time

#constants

#connect client
cytoscape = CyRestClient()
#delete all networks
cytoscape.network.delete_all()
#make network
network = cytoscape.network.create(name="Potasium-Sodium Pump")
#delete all old styles, make new one
cytoscape.style.delete_all()
cytoscape_style = cytoscape.style.create('new_style')
cytoscape.style.apply(cytoscape_style,network)


#classes & functions

class Node():
	"""
	Can make Nodes in cytoscape, perfect example of object-oriented programming!
	"""
	#set defaults
	node_color = "#89D0F5"
	node_shape = "ELLIPSE"
	node_loc_x = 0
	node_loc_y = 0
	node_loc_z = 0
	node_height = 50
	node_width = 50
	nodename = ""
	node_suid = 0
	node_bcolor = "BLACK"
	
	def __init__(self,nodename):
		"""
		init makes the node and grabs the view
		""" 
		self.nodename = nodename
		self.node = network.add_node(nodename)
		self.node_suid = self.node[nodename]
		self.view_id_list = network.get_views()
		self.view = network.get_view(self.view_id_list[0], format='view')
		self.view_functions = [fun for fun in dir(self.view) if not fun.startswith('_')]
		self.view_functions.sort()
		self.node_views = self.view.get_node_views()
		self.node_views_dict = self.view.get_node_views_as_dict()

	def changenode(self,color=node_color,shape=node_shape,width=node_width,height=node_height,label=nodename,bcolor=node_bcolor):
		"""
		changenode can change a few basic properties of the node
		"""
		#change self vars
		self.node_shape = shape
		self.node_color = color
		self.node_width = width
		self.node_height = height
		self.nodename = label
		self.node_bcolor = bcolor
		#change actual props
		self.view.update_node_views(visual_property='NODE_FILL_COLOR', values={self.node_suid: color})
		self.view.update_node_views(visual_property='NODE_SHAPE', values={self.node_suid: shape})
		self.view.update_node_views(visual_property='NODE_WIDTH', values={self.node_suid: width})
		self.view.update_node_views(visual_property='NODE_HEIGHT', values={self.node_suid: height})
		self.view.update_node_views(visual_property='NODE_LABEL', values={self.node_suid: label})
		self.view.update_node_views(visual_property='NODE_BORDER_PAINT', values={self.node_suid: bcolor})
		
		#uncomment print if you want node props
		#~ print(json.dumps(self.node_views_dict[self.node[self.nodename]], indent=0))

	def changenode_misc(self,node_prop,value):
		"""
		change misc props
		"""
		self.view.update_node_views(visual_property=node_prop, values={self.node_suid: value})

	def changepos(self,x=node_loc_x,y=node_loc_y):
		"""
		changepos changes the positioning of the node in cytoscape, relative to its own location
		"""
		#change self vars
		self.node_loc_x += x
		self.node_loc_y += y
		#change loc
		self.view.update_node_views(visual_property='NODE_X_LOCATION', values={self.node_suid: self.node_loc_x})
		self.view.update_node_views(visual_property='NODE_Y_LOCATION', values={self.node_suid: self.node_loc_y})
		#uncomment print if you need locations
		#~ print(self.node_loc_x,self.node_loc_y)

	def changepos_static(self,x,y):
		"""
		changepos changes the positioning of the node in cytoscape
		"""
		#change self vars
		self.node_loc_x = x
		self.node_loc_y = y
		#change loc
		self.view.update_node_views(visual_property='NODE_X_LOCATION', values={self.node_suid: x})
		self.view.update_node_views(visual_property='NODE_Y_LOCATION', values={self.node_suid: y})

	def change_layer(self,z):
		"""
		change layer of the node
		"""
		#change self vars
		self.node_loc_z = z
		#change layer
		self.view.update_node_views(visual_property='NODE_Z_LOCATION', values={self.node_suid: self.node_loc_z})

class Edge():
	
	def __init__(self,node1,node2):
		"""
		Makes edge between two nodes.
		"""
		#get suid of nodes
		suid1 = node1.node_suid
		suid2 = node2.node_suid
		#get view of network
		self.view_id_list = network.get_views()
		self.view = network.get_view(self.view_id_list[0], format='view')
		#connect nodes
		edges = network.add_edges(((suid1,suid2,"Sinewave"),(suid1,suid2,"Sinewave")))
		#for each edge, change property to Sinewave edge type
		for suid in edges.index:
			self.view.update_edge_views(visual_property='EDGE_LINE_TYPE', values={int(suid): 'Sinewave'})

#actual animation
"""
functions (work it harder)
"""
#place func
def pieces(steps):
	"""
	makes multiple nodes and returns them in a list
	"""
	#membraneloop
	nodelist = []
	for i in range(steps):
		nodelist.append(Node("Node"))
	return nodelist

def vertical_pieces_placing(steps,stepsize,nodelist,x,y):
	"""
	places a list of nodes in a vertical order
	"""
	for i in range(int((steps))):
		nodelist[i].changepos(x,y)
		y += stepsize

def edges_connect(listA,listB,):
	"""
	connects two lists of nodes to eachother
	"""
	for i in range(int((len(listA)+len(listB))/2)):
		Edge(listA[i],listB[i])
		

#animation func
def move_pump(node1,node2,node3,steps,step,delay,delay2):
	"""
	Moves the pump open and closes it after a delay
	"""
	move_pump_open(node1,node2,node3,steps,step,delay)
	time.sleep(delay2)
	move_pump_close(node1,node2,node3,steps,step,delay)

def move_pump_open(node1,node2,node3,steps,step,delay):
	"""
	opens the pump
	"""
	for i in range(steps):
		time.sleep(delay)
		node1.changepos(y=step)
		node2.changepos(y=-step)
		node3.changepos(y=step)

def move_pump_close(node1,node2,node3,steps,step,delay):
	"""
	closes the pump
	"""
	for i in range(steps):
		time.sleep(delay)
		node1.changepos(y=-step)
		node2.changepos(y=+step)
		node3.changepos(y=-step)

def move_in_steps(node,steps,stepx,stepy,delay):
	"""
	moves the nodes in steps with a delay, so it looks like an animation
	"""
	for i in range(steps):
		time.sleep(delay)
		node.changepos(x=stepx,y=stepy)

def K_Na_change(nodelist,i_begin,i_end,Kcolor,Kheight,Kwidth,Klabel):
	"""
	Changes appearance of the K and NA ions by going trough the node list
	"""
	while i_begin <= i_end:
		nodelist[i_begin].changenode(color=Kcolor,width=Kwidth,height=Kheight,label=Klabel)
		i_begin += 1

def animation1_placing_defining():
	"""
	Here the whole animation of the pump is made and ran
	"""
	
	
	"""
	create stuff (make it better)
	"""

	#make cel cytoplasm
	cytoplasm = Node("Cytoplasm")
	cytoplasm2 = Node("Cytoplasm")
	#make membranes 
	sideA = pieces(6)
	sideB = pieces(6)
	sideC = pieces(6)
	sideD = pieces(6)
	#make pump
	#big pieces
	pieceA = Node("A")
	pieceB = Node("B")
	#atp
	main_atp = Node("mATP")
	p_atp = Node("pATP")
	#ions
	K_N = pieces(10)
	

	"""
	place stuff (do it faster)
	"""

	#define & place membrane
	vertical_pieces_placing(6,30,sideA,-40,-200)
	vertical_pieces_placing(6,30,sideB,40,-200)
	vertical_pieces_placing(6,30,sideC,-40,50)
	vertical_pieces_placing(6,30,sideD,40,50)
	#make edges
	edges_connect(sideA,sideB)
	edges_connect(sideC,sideD)
	#place cytoplasm
	cytoplasm.changenode(shape="RECTANGLE",color="#ccf5ff",bcolor="#ccf5ff",width=1000,height=1000)
	cytoplasm.change_layer(-2)
	cytoplasm.changepos(540,0)
	cytoplasm2.changenode(shape="RECTANGLE",color="#ccf5ff",bcolor="#ccf5ff",width=100,height=80)
	cytoplasm2.change_layer(-1)
	cytoplasm2.changepos(0,0)

	#place pump
	pieceA.changenode(shape="ROUNDRECT",color="purple",width=108,height=60)
	pieceA.changepos(0,30)
	pieceA.change_layer(1)
	pieceB.changenode(shape="ROUNDRECT",color="purple",width=108,height=60)
	pieceB.changepos(0,-30)
	pieceB.change_layer(1)

	#place ATP
	main_atp.changenode(shape="OCTAGON",color="yellow",label="ATP",width=70,height=70)
	main_atp.changepos(1000,0)
	p_atp.changenode(shape="OCTAGON",color="#1a6365",label="P",width=30,height=30)
	p_atp.changepos(1000,-100)

	#place ions
	K_N[0].changepos(-160,-40)	#K1 move
	K_N[1].changepos(-130,40)	#K2 move
	K_N[2].changepos(-240,30)	#K3 
	K_N[3].changepos(-100,-100)	#K4
	K_N[4].changepos(-100,100)	#K5

	K_N[5].changepos(250,30)	#Na1 move#
	K_N[6].changepos(150,-120)	#Na2 move
	K_N[7].changepos(200,-150)	#Na3 move#
	K_N[8].changepos(240,80)	#Na4
	K_N[9].changepos(100,140)	#Na5
	#change ions
	K_Na_change(K_N,5,9,"#AEA8D3",20,20,"Na")
	K_Na_change(K_N,0,4,"#F1A9A0",20,20,"K")
	
	return K_N,pieceA,pieceB,p_atp,main_atp

	
def animation1_animate(moved_nodes):
	"""
	animate stuff (make us stronger)
	"""
	K_N,pieceA,pieceB,p_atp,main_atp = moved_nodes
	
	time.sleep(0.5)
	#NA ions


	#move NA ions
	#move1
	move_in_steps(K_N[5],185,-1,0,0.001)
	move_in_steps(K_N[5],10,0,-1,0.001)
	#move2
	move_in_steps(K_N[6],85,-1,1,0.001)
	move_in_steps(K_N[6],10,0,1,0.001)
	#move3
	move_in_steps(K_N[7],100,-1,1,0.001)
	move_in_steps(K_N[7],47,0,1,0.001)
	move_in_steps(K_N[7],35,-1,0,0.001)
	#open pump
	move_pump_open(pieceA,pieceB,p_atp,30,1,0.01)
	#move all 3 ions into pump
	for i in range(3):
		iets = i+5
		move_in_steps(K_N[iets],30,-1,0,0.001)
	#close pump
	move_pump_close(pieceA,pieceB,p_atp,30,1,0.01)

	#atp animation
	time.sleep(1)
	main_atp.changepos(-653,270)
	move_in_steps(main_atp,230,-1,-1,0.001)
	time.sleep(0.5)
	p_atp.changepos(-933,135)
	main_atp.changenode(color="orange",label="ADP",shape="OCTAGON")
	main_atp.changepos(-10,-3)
	time.sleep(0.5)
	move_in_steps(main_atp,230,1,-1,0.001)
	#atp animation

	#launch Na
	time.sleep(1)
	move_pump_open(pieceA,pieceB,p_atp,30,1,0.01)
	for i in range(3):
		iets = i+5
		move_in_steps(K_N[iets],70,-6,0,0.001)
	time.sleep(0.5)



	#K ions


	#move K ions
	#move1
	move_in_steps(K_N[0],40,1,1,0.001)
	move_in_steps(K_N[0],110,1,0,0.001)
	#move2
	move_in_steps(K_N[1],40,1,-1,0.001)
	move_in_steps(K_N[1],55,1,0,0.001)
	#close pump
	move_pump_close(pieceA,pieceB,p_atp,30,1,0.01)
	time.sleep(0.5)
	move_pump_open(pieceA,pieceB,p_atp,15,1,0.01)

	#shoot K towards nucleus, blow up cel
	for i in range(2):
		move_in_steps(K_N[i],70,6,0,0.001)

	#launch p
	move_in_steps(p_atp,230,1,1,0.001)
	#close pump

	move_pump_close(pieceA,pieceB,p_atp,15,1,0.01)
	time.sleep(1)
	
	"""
	put all moved nodes back in their startpos
	"""
	
	K_N[0].changepos_static(-160,-40)
	K_N[1].changepos_static(-130,40)
	K_N[5].changepos_static(250,30)
	K_N[6].changepos_static(150,-120)
	K_N[7].changepos_static(200,-150)
	main_atp.changepos_static(1000,0)
	p_atp.changepos_static(1000,-100)
	

#main

def main(argv=None):
	"""
	main, runs the whole program basically, its a pretty big deal.
	"""
	if argv ==None:
		argv = sys.argv 
	#work
	movednodes = animation1_placing_defining()
	while True:
		animation1_animate(movednodes)
	return 0

if __name__ == "__main__":
	print("its alive...   maybe")
	sys.exit(main())



